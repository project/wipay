CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Features
 * Installation
 * Special Notes
 * Disclaimer


INTRODUCTION
------------

Current Maintainer: Shivan Jaikaran <shivanj@gmail.com>

Wipay is an offsite payment gateway primary for the Caribbean.

This module integrates Wipay with Drupal Commerce, providing an offsite payment
gateway. Customers can make payments in your Drupal Commerce shop in a secure
way by being directed to the Wipay payment page and then being returned to your
website with success/failure.

Wipay is a simple way to accept payments online, allowing your customers to pay
with Visa, MasterCard - in Drupal Commerce.

FEATURES
------------
PCI compliance - The payment API is PCI compliant which help to keep your card
holder data safe and secure. Wipay takes on the responsibility of having the
systems and processes in place to prevent a data breach, relevant to all
merchants, regardless of revenue and credit card transaction volumes

INSTALLATION
------------
- Install as you would normally install any Drupal module. Composer is the
recommended way.

- Sign up for an account at Wipay. You will need to enter your Wipay account
number and API key in the module settings page.

- Wipay provides a "test" and "live" mode. You can toggle these settings in the
Drupal Commerce payment gateway config page.

- You MUST clear the cache after installing this module. THIS IS A VERY
IMPORANT STEP

SPECIAL NOTES
------------

1. Wipay API requires a phone number to complete the transaction. Without this,
the transaction will fail. So this module automatically creates a Phone field
using core's Telephone module. When the customer is on the payment page, they
will see this phone field. If you have an existing phone field prior to
installing this module, you will need to delete your pre-existing phone
field (or disable it) otherwise the customer will see 2 phone fields at
the payment page. Only the data from the phone field created with this module
is sent to Wipay (not your existing phone field). If there is a more elegant
way to solve this, please use the issue queue and provide a patch.

2. Wipay has a minimum order limit of $10. If the transaction value is less than
this, you will get a WSOD originating from Wipay. This module creates an Order
condition to cater for this but it is not enabled by default. You should
enable it or ensure all your prices are >= $10

3. Currency - the customer will be shown the currency of your Wipay account on
the Wipay page. The currency is tied to your Wipay account number. In other
words, it does not matter what currency you use in Drupal, when the customer
hits the Wipay page, it will automatically change to the Wipay currency in
your registered account. So you should ensure the currency is the same in
Drupal Commerce as your Wipay account.

4. Wipay Fees - The total amount of the order is sent to Wipay. This is the final
price the customer will pay. However, Wipay (just like Paypal), will subtract
their commission fee from what the customer pays. So if you (the merchant)
want to incur the fee, there is nothing you need to do. If you want to force
the customer to pay the Wipay fees, then you have to use Commerce Fees
[https://www.drupal.org/project/commerce_fees] and add 2 fees which are a fixed
fee of $2 and also 3%. Using Commerce Fees, you will have to create 2 separate
fees and the customer will see 2 fees added on the final payment page.
Currently Commerce Fees does not provide a way to create 1 fee with both a
fixed price and percentage. I create a patch for that. See: #3151656:
Adding a fee based on Percentage AND Fixed Amount
[https://www.drupal.org/project/commerce/issues/3151656]

DISCLAIMER
------------
The maintainers and contributors of this module are not experts in PCI-DSS
compliance. Your compliance with PCI-DSS requirements is entirely your
responsibility. Since this payment gateway is an offsite payment gateway,
the PCI compliance is handled at Wipay. At a bare minimum, your local website
should be using HTTPS.

This project is not affiliated with Wipay Caribbean and no financial
sponsorship has been provided by Wipay for its development. Use this
module at your own risk.

Use the issue tracker for bug reports or questions about Drupal integration.
If you want more info about Wipay, visit https://wipaycaribbean.com/

More information about Wipay's API can be found here:
[https://wipaycaribbean.com/credit-card-documentation/]

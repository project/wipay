<?php

namespace Drupal\wipay\Plugin\Commerce\Condition;

use Drupal\commerce\Plugin\Commerce\Condition\ConditionBase;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\commerce_price\Price;
use Drupal\Core\Url;

/**
 * Wipay limits transactions to >= $10
 *
 * @CommerceCondition(
 *   id = "order_minimum_order_ten",
 *   label = @Translation("Minimum order of 10"),
 *   display_label = @Translation("Minimum order must be greater than or equals to $10"),
 *   category = @Translation("Order", context = "Commerce"),
 *   entity_type = "commerce_order",
 * )
 */
class MinimumOrderTen extends ConditionBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function evaluate(EntityInterface $entity) {
    $this->assertEntity($entity);
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $entity;
    $total_price = $order->getTotalPrice();
    if (!$total_price) {
      return FALSE;
    }
    $condition_price = Price::fromArray(['number'=> '10', 'currency_code' => 'TTD']);

    if ($total_price->lessThan($condition_price)) {
      $this->messenger()->addError($this->t('Due to limitations of our payment gateway Wipay, the order total must be greater than $10. <a href=":url">Please continue shopping.</a>',
                                            [':url' => Url::fromUri('internal:/store/products')->toString()]
                                          ));
      return FALSE;
    }
    else return TRUE;
  }
}

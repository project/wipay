<?php

namespace Drupal\wipay\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\Entity\PaymentGatewayInterface;
use Drupal\commerce_payment\Entity\PaymentGateway;

/**
 * Provides the Wiipay offsite Checkout payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "wipay_redirect_checkout",
 *   label = @Translation("Wipay Redirect to wipay"),
 *   display_label = @Translation("Wipay"),
 *    forms = {
 *     "offsite-payment" = "Drupal\wipay\PluginForm\RedirectCheckoutForm",
 *   },
 *   modes = {"test" = @Translation("Test"), "live" = @Translation("Live")},
 *   payment_method_types = {"credit_card"},
 *   credit_card_types = {
 *     "mastercard", "visa",
 *   },
 * )
 */
class WipayCheckout extends OffsitePaymentGatewayBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
        'account_number' => '',
        'api_key' => '',
      ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['account_number'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Wipay account_number'),
      '#description' => $this->t('This is your Wipay account number.'),
      '#default_value' => $this->configuration['account_number'],
      '#required' => TRUE,
    ];

    $form['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Wipay API key'),
      '#description' => $this->t('Your Wipay API key.'),
      '#default_value' => $this->configuration['api_key'],
      '#required' => TRUE,
    ];

    return $form;
  }

  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $values = $form_state->getValue($form['#parents']);
    $this->configuration['account_number'] = $values['account_number'];
    $this->configuration['api_key'] = $values['api_key'];
  }

  /**
   * {@inheritdoc}
   */
  public function onReturn(OrderInterface $order, Request $request) {

    //success
    /*
    http://your_return_url/?status=success
    &name=John%20Doe
    &email=customer@gmail.com
    &hash=129c8b887ed83ec36523843afcd51caa
    &order_id=4398
    &transaction_id=100-1-4398-20170108100202
    &reasonCode=1
    &reasonDescription=Transaction%20is%20approved.
    &responseCode=1
    &total=10
    &D=TT
    &date=10%3A02%3A17p
    */

    //failure
    /*
    http://your_return_url/?status=failed
    &name=John%20Doe
    &email=customer@gmail.com
    &order_id=4398
    &transaction_id=100-1-4398-20170108100202
    &reasonCode=2
    &reasonDescription=Transaction%20Declined.
    &responseCode=2
    &total=10
    &D=TT
    &date=10%3A02%3A17pm
    */

    $status = $request->query->get('status'); //success or failed

    $returned_hash = $request->query->get('hash');

    $wipay_order_id = $request->query->get('order_id');

    $total = $request->query->get('total');

    $config = $this->getConfiguration();

    $reason_description = $request->query->get('reasonDescription');

    /** @var \Drupal\commerce_payment\Entity\EntityWithPaymentGatewayInterface $payment_gateway */
    $payment_gateway = \Drupal::entityTypeManager()->getStorage('commerce_payment_gateway')->loadByProperties(['plugin' => 'wipay_redirect_checkout']);

    $wipay_gateway = array_shift($payment_gateway);

    /** @var \Drupal\commerce_payment\Entity\PaymentGatewayInterface $payment_gateway_config */
    $payment_gateway_config = $wipay_gateway->getPluginConfiguration();

    $mode = $payment_gateway_config['mode']; //live or test

    if ($mode == 'test') {
      $api_key = '123';
    }

    else $api_key = $config['api_key'];

    $hash = md5($wipay_order_id . $total . $api_key);

    if($hash == $returned_hash) {

        \Drupal::logger('wipay')->info('New order: @ordernum. <a href = ":orderurl">View Order</a> ',
       array(
           '@ordernum' => $wipay_order_id,
           ':orderurl' => \Drupal::request()->getHost().'/admin/commerce/orders/'.$wipay_order_id,
       ));

    }

    else {
      throw new PaymentGatewayException('Hash did not match.');
    }

    if ($status == 'failed') {
      throw new PaymentGatewayException('Payment failed from Wipay. Reason given: @reason',
      array('@reason'=> $reason_description));
    }

  }

}

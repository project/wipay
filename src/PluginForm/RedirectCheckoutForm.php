<?php

namespace Drupal\wipay\PluginForm;

use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm as BasePaymentOffsiteForm;
use Drupal\Core\Form\FormStateInterface;

class RedirectCheckoutForm extends BasePaymentOffsiteForm {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    /** @var \Drupal\commerce_payment\Entity\EntityWithPaymentGatewayInterface $payment_gateway */
    $payment_gateway = $this->entity;

    /** @var \Drupal\commerce_payment\Entity\PaymentGatewayInterface $payment_gateway_config */
    $payment_gateway_config = $payment_gateway->getPaymentGateway()->getPluginConfiguration();

    $mode = $payment_gateway_config['mode']; //live or test

    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;

    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $payment->getOrder();

    $phone = $order->getBillingProfile()->get('wipay_field_phone')->getString(); //652-7895

    $sane_phone = $this->simpleSanitize($phone); // 6527895

    /** @var Drupal\address\Plugin\Field\FieldType\AddressItem  $address_item */
    $address_item = $order->getBillingProfile()->get('address')->getIterator();

    $first_name = $address_item[0]->getGivenName();
    $last_name = $address_item[0]->getFamilyName();

    //customer information
    $data['name'] = $first_name . ' ' . $last_name;
    $data['email'] = $order->getEmail();
    $data['phone'] = $sane_phone;

    if ($mode == 'live') {
      $wipay_url = 'https://wipayfinancial.com/v1/gateway_live';
      $developer_id = $payment_gateway_config['account_number'];
    }
    else {
      $wipay_url = 'https://sandbox.wipayfinancial.com/v1/gateway';
      $developer_id = 1;
    }

    //order information
    $data['total'] = $payment->getAmount()->getNumber();
    $data['order_id'] = $payment->getOrderId();
    $data['developer_id'] = $developer_id;
    $data['return_url'] = $form['#return_url'];


    return $this->buildRedirectForm(
      $form,
      $form_state,
      $wipay_url,
      $data,
      parent::REDIRECT_POST
    );
  }

  /**
  * @return string
  * Takes a phone number like 653-2032 and trims the spaces and removes dashes
  **/
  private function simpleSanitize($phonestring) {
    //Make alphanumeric (removes all other characters)
    $string = preg_replace("/[^a-z0-9_\s-]/", "", trim($phonestring));
    //Clean up multiple dashes or whitespaces
    $string = preg_replace("/[\s-]+/", "", $string);

    return $string;

  }
}

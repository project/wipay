<?php

use Drupal\field\FieldStorageConfigInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Implements hook_ENTITY_TYPE_access().
 *
 * Forbids the profile "phone" field from being deletable.
 * This is an alternative to locking the field which still leaves
 * the field editable.
 */
function wipay_field_storage_config_access(FieldStorageConfigInterface $field_storage, $operation) {
  if ($field_storage->id() == 'profile.wipay_field_phone' && $operation == 'delete') {
    return AccessResult::forbidden();
  }
  return AccessResult::neutral();
}

/**
 * Implements hook_entity_operation_alter().
 *
 * Hides the "Storage settings" operation for the profile "phone" field.
 */
function wipay_entity_operation_alter(array &$operations, EntityInterface $entity) {
  if ($entity->getEntityTypeId() == 'field_config') {
    /** @var \Drupal\Core\Field\FieldConfigInterface $entity */
    if ($entity->getTargetEntityTypeId() == 'profile' && $entity->getName() == 'wipay_field_phone') {
      unset($operations['storage-settings']);
    }
  }
}
